c:\python34\python.exe setup.py build %1
if exist build\lib.win32-3.4\wissen\_wissen.pyd (
	del /f /q wissen\extension\_wissen.pyd.34x86
	copy build\lib.win32-3.4\wissen\_wissen.pyd wissen\extension\_wissen.pyd.34x86
)	

if exist build\lib.win-amd64-3.4\wissen\_wissen.pyd (
	del /f /q wissen\extension\_wissen.pyd.34x64
	copy build\lib.win-amd64-3.4\wissen\_wissen.pyd wissen\extension\_wissen.pyd.34x64
)
rem del /s /q build
rem rmdir /s /q build
