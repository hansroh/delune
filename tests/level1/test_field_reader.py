import pytest
import delune
from delune.fields import *
from delune.searcher.segment import fieldreader, typeinfo

def render (f, type, qstr, lang = "en"):
    try:
        name, val = qstr.split (":")
    except ValueError:
        name, val = "default", qstr            
    return f (type, qstr, lang, name, val, 0)
     
def test_string (freader):
    f = render (freader, None, "title:minds")
    assert f.value == "minds"
    
    f = render (freader, STRING, "`criminal minds")
    assert f.value == "criminal minds"
    
    f = render (freader, STRING, "criminal minds")
    assert f.value == "criminal minds"
    
    f = render (freader, STRING, "1010*")
    assert isinstance (f, fieldreader.FieldWildCard)
    assert isinstance (f.value [0], fieldreader.Field)
    assert f.value [0].value == None
    
    f = render (freader, STRING, "스노우보드")
    assert f.value == "스노우보드"
    
    f = render (freader, STRING, "`알파인 스노우보드")
    assert f.value == "알파인 스노우보드"
    
def test_term (freader):        
    f = render (freader, TERMSET, "`criminal minds")
    assert isinstance (f, fieldreader.Phrase)
    assert len (f.value) == 2
    assert f.value [0].value == "crimin"
    
    f = render (freader, TERMSET, "criminal minds")
    assert isinstance (f, fieldreader.TermList)
    assert len (f.value) == 2
    assert f.value [1].value == "mind"
    
    f = render (freader, TERMSET, "crimin*")
    assert isinstance (f, fieldreader.WildCard)
    assert len (f.value) == 1
    assert f.value [0].value == None
    
    f = render (freader, TERMSET, "스노우보드", "ko")
    assert isinstance (f, fieldreader.TermList)
    assert len (f.value) == 4
    assert f.value [1].value == "노우"
    
    f = render (freader, TERMSET, "`알파인 스노우보드", "ko")
    assert isinstance (f, fieldreader.Phrase)
    assert len (f.value) == 6
    assert f.value [1].value == "파인"

def test_phrase (freader):    
    f = render (freader, TEXT, "criminal")
    assert isinstance (f, fieldreader.TermList)
    assert len (f.value) == 1
    assert f.value [0].value == "crimin"
    assert isinstance (f.value [0], fieldreader.Term)
    
    f = render (freader, TEXT, "criminal minds")
    assert isinstance (f, fieldreader.TermList)
    assert len (f.value) == 2
    assert f.value [0].value == "crimin"
    assert isinstance (f.value [0], fieldreader.Term)
    
    f = render (freader, TEXT, "`criminal minds")
    assert isinstance (f, fieldreader.Phrase)
    assert len (f.value) == 2
    assert f.value [0].value == "crimin"
    
    f = render (freader, TEXT, "criminal-minds")
    assert isinstance (f, fieldreader.Phrase)
    assert len (f.value) == 2
    assert f.value [0].value == "crimin"
    assert isinstance (f.value [0], fieldreader.Term)
    
    f = render (freader, TEXT, "ac/dc")
    assert isinstance (f, fieldreader.Phrase)
    assert len (f.value) == 1
    assert f.value [0].value == "ac/dc"
    assert isinstance (f.value [0], fieldreader.Term)
    
    f = render (freader, TEXT, "ac-dc")
    assert isinstance (f, fieldreader.Phrase)
    assert len (f.value) == 1
    assert f.value [0].value == "ac-dc"
    assert isinstance (f.value [0], fieldreader.Term)
    
    f = render (freader, TEXT, "`criminal^4 minds")
    assert isinstance (f, fieldreader.Phrase)
    assert len (f.value) == 2
    assert f.value [0].value == "crimin"
    assert isinstance (f.value [0], fieldreader.Term)
    
def test_coord (freader):    
    f = render (freader, COORD, "loc:56.7/127.5~100")
    assert isinstance (f, fieldreader.Coord)
    assert f.value == (2367000, 3075000, 100)
    
    f = render (freader, COORD, "loc:56.7/-127.5~100")
    assert isinstance (f, fieldreader.Coord)
    assert f.value == (2367000, 525000, 100)
        
    f = render (freader, COORD8, "loc:56.7/127.5~100")
    assert isinstance (f, fieldreader.Coord)
    assert f.value == (23670000000, 30750000000, 100)
    
    with pytest.raises (ValueError):
        f = render (freader, COORD8, "loc:56.7,560.5~100")
    with pytest.raises (ValueError):
        f = render (freader, COORD8, "loc:56.7,560.5")

def test_digit (freader):    
    f = render (freader, BIT8, "flag:01100000")
    assert isinstance (f, fieldreader.Digit)
    assert f.value == (96, 'all')
    
    f = render (freader, BIT32, "flag:01100000")
    assert isinstance (f, fieldreader.Digit)
    assert f.value == (96, 'all')
    
    f = render (freader, BIT32, "flag:01100000/any")
    assert isinstance (f, fieldreader.Digit)
    assert f.value == (96, 'any')
    
    f = render (freader, INT32, "flag:10~50")
    assert isinstance (f, fieldreader.Digit)
    assert f.value == (10, 50)
    
    f = render (freader, FNUM+"4.3", "flag:10~50")
    assert isinstance (f, fieldreader.FieldRange)
    assert f.value [0].value == None
    
    assert typeinfo.zfill (FNUM+"10.3", 0.45) == "000000.450"
    assert typeinfo.zfill (FNUM+"10.3", -0.45) == "-000000.450"
    assert typeinfo.zfill (FNUM+"10.3", 120.45) == "000120.450"
    assert typeinfo.zfill (FNUM+"10.3", -120.45) == "-000120.450"
    