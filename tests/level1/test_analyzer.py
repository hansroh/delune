# coding=utf8

from delune import standard_analyzer

def test_EN ():
	d = "television billing services boxes x-ray c++ c# it"
	a = standard_analyzer (10000)		
	a.setopt (ngram = 0, stem_level = 0)
	assert " ".join (a.query (d)) == 'television billing services boxes x-ray c++ c#'	
	a.setopt (ngram = True, stem_level = 1)
	assert " ".join (a.query (d)) == 'television billing servic box x-ray c++ c#'
	a.setopt (ngram = 0, stem_level = 1)
	assert " ".join (a.query (d)) == 'television billing servic box x-ray c++ c#'	
	a.setopt (ngram = True, stem_level = 2)
	assert " ".join (a.query (d)) == 'televis bill servic box x-ray c++ c#'
	a.setopt (ngram = 0, stem_level = 2)
	assert " ".join (a.query (d)) == 'televis bill servic box x-ray c++ c#'
	a.close ()

def test_KR ():
	d = "television 알파인보드 날 날개"
	a = standard_analyzer (10000)		
	a.setopt (ngram = False, stem_level = 0)
	assert " ".join (a.query (d, "en")) == 'television 알파인보드 날 날개'
	a.setopt (ngram = True, stem_level = 0)
	assert " ".join (a.query (d, "en")) == 'television _알파 알파인 파인보 인보드 보드_ 날 날개'
	a.setopt (ngram = True, stem_level = 0)
	assert " ".join (a.query (d, "en")) == 'television _알파 알파인 파인보 인보드 보드_ 날 날개'	
	a.setopt (ngram = True, stem_level = 0)
	assert " ".join (a.query (d)) == 'television _알파 알파인 파인보 인보드 보드_ 날 날개'
	a.setopt (stem_level = 1)
	assert " ".join (a.query (d, "ko")) == 'television _알 알파 파인 인보 보드 드_ 날 _날 날개 개_'
	a.setopt (stem_level = 2)
	assert " ".join (a.query (d, "ko")) == 'televis _알 알파 파인 인보 보드 드_ 날 _날 날개 개_'
	a.close ()


def test_NgramSpace ():
	d = "television 알파인보드 날 날개"
	a = standard_analyzer (10000)		
	a.setopt (ngram = True, stem_level = 0, ngram_no_space = 1)
	assert " ".join (a.query (d)) == 'television _알파 알파인 파인보 인보드 보드_ 날 날개'
	
	a.setopt (ngram = True, stem_level = 0, ngram_no_space = 1)
	assert " ".join (a.query (d, "ko")) == 'television _알 알파 파인 인보 보드 드_ 드날 날 날날 _날 날개 개_'
	
	a.close ()


def test_Endwords ():	
	d = "television 날개입니다. 일반영어"
	a = standard_analyzer (10000)		
	a.setopt (endwords = ["입니다", "구요", "영어"])
	
	a.setopt (ngram = True, stem_level = 2, ngram_no_space = 1)
	assert " ".join (a.query (d)), 'televis _날 날개 개_ _일 일반 반_'
	
	a.setopt (ngram = False, stem_level = 1, ngram_no_space = 1)
	assert " ".join (a.query (d)), 'television 날개 일반'
	
	a.close ()

def test_Stopwords ():	
	a = standard_analyzer (10000)			
	assert " ".join (a.query ("It")), 'It'			
	a.setopt (stopwords_case_sensitive = 0)
	assert " ".join (a.query ("IT")) == ''
	a.setopt (stopwords = [])
	assert " ".join (a.query ("it")), 'it'

def test_HTMLStrip ():	
	d = "<b href='sfsdf'>files</a>"
	a = standard_analyzer (10000)		
	a.setopt (strip_html = True)		
	assert a.term (d) == [('file', 1)]
	
	d = """
		<script>
		document.location.href = "/";
		</script>
	"""
	assert a.term (d) == []
	
	d = """
		<script>
		//</script>
		document.location.href = "/";
		</script>
	"""
	assert a.term (d) == []
	
	d = """
		<script>
		/*
			</script>myabe 
			<i>uhulla</i>
		*/
		document.location.href = "/";
		</script>
		
		<b>Success</b>
	"""
	assert a.term (d) == [('Success', 1)]
	
	a.close ()

def test_UCase ():	
	a = standard_analyzer (10000)	
	a.setopt (stem_level = 2)
	assert " ".join (a.query ("SERVICES BOXES")) == 'SERVICES BOXES'
	assert " ".join (a.query ("Services Boxes Billing")) == 'Servic Box Bill'
	a.setopt (make_lower_case = True)
	assert " ".join (a.query ("SERVICES BOXES")) == 'servic box'
	assert " ".join (a.query ("Services Boxes Billing")) == 'servic box bill'
	

	