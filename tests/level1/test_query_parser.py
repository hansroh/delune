from delune.searcher.querylexer import  QueryLexer
from delune.searcher.queryparser import  ArithmeticParser
import pytest

def lex (qs):
    return QueryLexer (qs).tokenize ()
    
def exp (tokens):
    return ArithmeticParser(tokens).parse_expression()


def test_lexer ():    
    ts = lex ('home:56.14343/-4.1~10.45')
    assert ts == ['(', 'home:56.14343/-4.1~10.45', ')', 'EOF']
    
    ts = lex ('a')
    print (ts)
    assert ts == ['a', 'EOF']
    
    ts = lex ('a/b a-b')
    print (ts)
    assert ts == ['a/b', '*', 'a-b', 'EOF']
    
    ts = lex ('home:56.14343/-4.1~10.45 cat:45,90')
    assert ts == ['(', 'home:56.14343/-4.1~10.45', ')', '*', '(', 'cat:45', '+', 'cat:90', ')', 'EOF']
    assert exp (ts) == ['*', 'home:56.14343/-4.1~10.45', ['+', 'cat:45', 'cat:90']]
    
    with pytest.raises (ValueError):
        ts = lex ('-porsche')
    
    ts = lex ('cars -porsche')
    ts == ['cars', '-', 'porsche', 'EOF']
    assert exp (ts) == ['-', 'cars', 'porsche']
    
    ts = lex ('cars -"toyota car"')
    print (ts)
    ts == ['cars', '-', '`toyota car', 'EOF']
    assert exp (ts) == ['-', 'cars', '`toyota car']

    ts = lex ('cars -("toyota car")')
    print (ts)
    ts == ['cars', '-', '`toyota car', 'EOF']
    assert exp (ts) == ['-', 'cars', '`toyota car']
    
    ts = lex ('cars -("toyota car" or bugatti)')
    print (ts)
    ts == ['cars', '-', '(', '`toyota car', '+', 'bugatti', ')', 'EOF']
    assert exp (ts) == ['-', 'cars', ['+', '`toyota car', 'bugatti']]
    
    ts = lex ('cars -("toyota car", bugatti)')
    print (ts)
    ts == ['cars', '-', '(', '`toyota car', '*', ',', '*', 'bugatti', ')', 'EOF']
    assert exp (ts) == ['-', 'cars', ['*', ['*', '`toyota car', ','], 'bugatti']]
    
    ts = lex ('cars -("toy car" mk:kia,bug,"aka dia")')
    print (ts)
    ts == ['cars', '-', '(', '`toy car', '*', '(', 'mk:kia', '+', 'mk:bug', '+', 'mk:`aka dia', ')', ')', 'EOF']
    assert exp (ts) == ['-', 'cars', ['*', '`toy car', ['+', ['+', 'mk:kia', 'mk:bug'], 'mk:`aka dia']]]
    
    ts = lex ('(alltitle:"medical^3 service" -monitoring) file:"adada sada" -file:"qwe qwe-q"')
    print (ts)
    ts == ['(', '(', 'title:`medical^3 service', ')', '-', 'title:monitoring', ')', '*', '(', 'file:`adada sada', ')', '-', '(', 'file:`qwe qwe-q', ')', 'EOF']
    assert exp (ts) == ['-', ['*', ['-', 'title:`medical^3 service', 'title:monitoring'], 'file:`adada sada'], 'file:`qwe qwe-q']
    