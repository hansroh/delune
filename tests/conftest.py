import os
import pytest
import csv
from delune.fields import *
import shutil
from rs4 import partial
import skitai
from app import mount_on_fly
import atila
import delune
from atila.pytest_hooks import *

os.environ ["SKITAI_ENV"] = "PYTEST"

@pytest.fixture
def rdir ():
    r = os.path.join (os.path.dirname (__file__), "resources")
    if os.path.isdir (r):
        shutil.rmtree (r)
    yield r
    if os.path.isdir (r):
        shutil.rmtree (r)

@pytest.fixture
def launch (dryrun):
    return partial (skitai.test_client, port = 30371, silent = False, dry = dryrun)

def _read (fn):
    all = {}
    with open (os.path.join (os.path.dirname (__file__), "dataset", fn)) as fp:
        rd = csv.reader(fp, delimiter=',', quotechar='"', skipinitialspace = True)
        for row in rd:
            code, title = row
            if not code or not title:
                continue
            all [code] = title
    return all

@pytest.fixture (scope = "session")
def tempdir (tmpdir_factory):
    t = tmpdir_factory.mktemp ("delune")
    return t.dirname

@pytest.fixture
def naics ():
    r = _read ("naics2007.csv")
    assert len (r) == 2328
    return r

@pytest.fixture
def _index ():
    return __index

@pytest.fixture
def cname ():
    return "pytest-temp"

@pytest.fixture
def app (rdir):
    with skitai.preference () as pref:
        pref.overrides (mount_on_fly)
        pref.config.resource_dir = rdir
        pref.config.MAINTAIN_INTERVAL = 1
        with atila.load (delune, pref) as app:
            yield app

def __index (col, naics, commit_interval = 0, docid = True, count = 0):
    with col.documents as ds:
        rows = 0
        for code, title in sorted (list (naics.items ())):
            d = ds.new (docid and code or None)
            d.add_content ({"id": code, "title": title, 'rownum': rows})
            assert d.stored [0] == {"id": code, "title": title, 'rownum': rows}
            d.add_content ({"full": "{} - {}".format (code, title)})
            assert d.stored [1] == {"full": "{} - {}".format (code, title)}

            d.set_snippet (title, option = "strip_hrml")
            assert d.summary [0] == title
            assert d.summary [1] == "en"
            assert d.summary [2] == ["strip_hrml"]

            d.add_field ("default", "$content-1.full", TEXT)
            d.add_field ("code", "$content-0.id", STRING)
            d.add_field ("term", "$content-1.full", TERMSET)
            d.add_field ("icode", "$content-0.rownum", FNUM, format = "10.2")
            d.add_field ("rownum", "$content.rownum", INT32)
            d.add_field ("rownum16", "$content.rownum", INT16)
            d.add_field ("rownumbit",  rows, BIT32)
            d.add_field ("rownumbit16",  rows, BIT16)
            d.add_field ("coord", "{}/{}".format (rows % 89, rows % 178), COORD)
            ds.add (d)
            if commit_interval and rows % commit_interval == 0:
                col.commit ()
            rows += 1
            if count and rows == count:
                break

