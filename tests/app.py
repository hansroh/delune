#!/usr/bin/python3

import os
import delune
import skitai
import ipaddress as ip
import sys
from delune.searcher.searcher import Searcher
import atila
import requests
import time

def mount_on_fly (app, options):
	@app.permission_check_handler
	def permission_check_handler (context, perms):
		raddr = context.request.get_remote_addr ()
		if raddr == "125.177.82.99":
			return
		if raddr.startswith ("127.0.") or raddr.startswith ("192.168."):
			return
		if raddr.startswith ("172.31."):
			return
		if ip.IPv4Address(raddr) in ip.IPv4Network('220.117.202.0/28'):
			return
		return context.response.Fault ("403 Permission Denied")

app = atila.Atila (__name__)

@app.route ('/1')
def delune_async (context):
	r = requests.get (f'http://127.0.0.1:30371/delune/cols/testcol/documents?q=service&limit=2').json ()
	return context.API (r)

@app.route ('/2')
def delune_async2 (context):
	r = requests.get (f'http://127.0.0.1:30371/delune/cols/testcol/documents?q=service&limit=2').json ()
	return context.API (r)

@app.route ('/3')
def delune_async3 (context):
	r = requests.get (f'http://127.0.0.1:30371/delune/cols/testcol/documents?q=service&limit=2').json ()
	return context.API (r)

@app.route ('/4')
def delune_async4 (context):
	return context.API (result = app.g.event_status)

@app.mounted
def mounted (context):
	app.g.event_status = []

@app.on ('delune:reconfigure')
def reconfigure (context, alias):
	app.g.event_status.append ('reconfigure')

@app.on ('delune:commit')
def commit (context, alias):
	app.g.event_status.append ('commit')


if __name__ == "__main__":
	with skitai.preference () as pref:
		pref.overrides (mount_on_fly)
		config = pref.config
		config.resource_dir = skitai.joinpath ('resources')
		config.MAINTAIN_INTERVAL = 1
		Searcher.MAINTERN_INTERVAL = 1
		skitai.mount ("/delune", delune, "app", pref)

	skitai.mount ("/test", app, name = 'myapp', subscribe = 'delune')

	workers = int (skitai.getsysopt ("workers", 1))
	threads = int (skitai.getsysopt ("threads", 4))
	skitai.run (
		name = "delune",
		workers = int (skitai.getsysopt ("workers", 1)),
		threads = int (skitai.getsysopt ("threads", 4)),
		port = 9005
	)
