set -xe

pytest -x $1 $2 $3 level1
pytest -x $1 $2 $3 level2
pytest -x $1 $2 $3 level3
pytest -x $1 $2 $3 level4
pytest -x $1 $2 $3 level5
pytest -x $1 $2 $3 level6
