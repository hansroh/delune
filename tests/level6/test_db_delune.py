import os
import pytest
import os
import csv
import shutil
import delune
from delune.fields import *
from rs4 import pathtool, logger
from delune.cli import indexer
import shutil
import time
import rs4

def test_skitai_db_class (naics, rdir, launch, _index):
    delune.configure (1, logger.screen_logger ()) # for indexer
    confdir = os.path.join (rdir, "delune", "config")
    coldir = os.path.join (rdir, "delune", "collections")

    with launch (os.path.join (os.path.dirname (os.path.dirname (__file__)), "app.py")) as engine:
        dl = delune.mount ("http://localhost:30371/delune")
        with dl.create ("testcol", ["testcol"]) as col:
            col.setopt ("analyzer", make_lower_case = True, stem_level = 1)
            _index (col, naics, 100)
            indexer.index (rdir)
            col.commit ()
            col.close ()

        with dl.load ("testcol") as col:
            r = col.search ("service")
            assert "id" in r ["result"][0][0]

        r = engine.get ("/delune/status"); time.sleep (2)
        assert r.status_code == 200

        r = engine.get ("/test/1")
        assert r.status_code == 200
        assert "id" in r.data ["result"][0][0]
        assert len (r.data ["result"]) == 2

        r = engine.get ("/test/2")
        assert r.status_code == 200
        assert "id" in r.data ["result"][0][0]
        assert len (r.data ["result"]) == 2

        r = engine.get ("/test/3")
        assert r.status_code == 200
        assert "id" in r.data ["result"][0][0]
        assert len (r.data ["result"]) == 2

        with rs4.stub ("http://localhost:30371") as stub:
            r = stub.get ("/test/{}", 3)
            assert r.status_code == 200
            assert "id" in r.json () ["result"][0][0]

        with rs4.stub ("http://localhost:30371/test") as stub:
            r = stub.get ("/3")
            assert r.status_code == 200
            assert "id" in r.json () ["result"][0][0]

        col.drop (True)
        assert not os.path.isdir (os.path.join (confdir, "testcol"))
