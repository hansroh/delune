import delune
import os
import skitai
import atila
from delune.cli import local
import shutil
import time

def test_collections_async (app, rdir, cname):
    app.config.MAINTAIN_INTERVAL = 1

    with app.test_client () as cli:
        api = cli.api ()

        r = cli.get ("status")
        r = api.cols ("pytest-temp").get ()
        assert r.status_code == 404

        config = local.make_config (cname, [cname])
        r = api.cols (cname).post (config)
        assert r.status_code == 202

        r = api.cols (cname).post (config)
        assert r.status_code == 202

        for i in range (3):
            r = cli.get ("status"); time.sleep (1)
        r = api.cols (cname).post (config)
        assert r.status_code == 406

        r = api.cols (cname).delete (side_effect = "data")
        assert r.status_code == 202

        r = api.cols (cname).post (config)
        assert r.status_code == 406

        r = cli.get ("status"); time.sleep (2)

        r = api.cols (cname).get ()
        assert r.status_code == 404
