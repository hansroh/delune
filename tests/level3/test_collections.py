import delune
import os
import skitai
import atila
from delune.cli import local
import shutil
import time

def test_collections (app, rdir, cname):
    with app.test_client () as cli:
        r = cli.get ("status")
        assert r.status_code == 200

        api = cli.api ("/")
        r = api.get ()
        assert r.status_code == 200

        r = api.cols ("pytest-temp").get_ ()
        assert r.status_code == 404

        r = api.cols.get ()
        assert "mounted_dir" in r.data
        assert "collections" in r.data

        config = local.make_config (cname, [cname])
        r = api.cols (cname).post (config, side_effect = "now")
        assert r.status_code == 201

        r = api.cols (cname).post (config, side_effect = "now")
        assert r.status_code == 406

        r = api.cols (cname).delete (side_effect = "data now")
        assert r.status_code == 204

        assert os.path.isfile (os.path.join (rdir, "delune", "config", "-pytest-temp"))
        r = api.cols (cname).delete ()
        assert r.status_code == 404
        os.remove (os.path.join (rdir, "delune", "config", "-pytest-temp"))

        r = api.cols (cname).post (config, side_effect = "now")
        assert r.status_code == 201
        r = api.cols (cname).delete (side_effect = "now")
        assert r.status_code == 204
        assert os.path.isfile (os.path.join (rdir, "delune", "config", "#pytest-temp"))
        r = api.cols (cname).delete (side_effect = "now")
        assert r.status_code == 404
        os.remove (os.path.join (rdir, "delune", "config", "#pytest-temp"))

        r = api.cols (cname).post (config, side_effect = "now")
        assert r.status_code == 201
        assert "data_dir" in r.data

        r = api.cols (cname).put (config)
        assert r.status_code == 200

        r = api.cols (cname).patch ( {"section": "analyzer", "data": {"stem_level": 2, "make_lower_case": True} })
        assert r.status_code == 200
        assert r.data ["analyzer"]["stem_level"] == 2

        r = api.cols (cname).config.get ()
        assert r.status_code == 200
        assert r.data ["analyzer"]["stem_level"] == 2

        r = api.cols (cname).config.get ()
        assert r.status_code == 200
        assert r.data ["analyzer"]["stem_level"] == 2

        r = api.cols ("pytest-temp").get ()
        assert "segmentinfos" in r.data

        r = api.cols ("pytest-temp").commit.post ({})
        assert r.status_code == 205

        r = api.cols ("pytest-temp").rollback.post ({})
        assert r.status_code == 205

        r = api.cols ("pytest-temp").stem.get (q = "medical services", lang = "en")
        assert r.data == {'medical services': 'medic servic'}
        assert r.status_code == 200

        r = api.cols ("pytest-temp").stem.get (q = "medical, services", lang = "en")
        assert r.data == {'medical': 'medic', ' services': 'servic'}

        r = api.cols ("pytest-temp").stem.post ({"q": "medical, services", "lang": "en"})
        assert r.data == {'medical': 'medic', ' services': 'servic'}

        r = api.cols ("pytest-temp").analyze.post ({"q": "medical, services", "lang": "en"})
        assert r.data == {'medic': [0], 'servic': [1]}

        #---------------------------------------------------

        r = api.cols (cname).delete (side_effect = "data now")
        assert r.status_code == 204
        for i in range (3):
            r = api.cols ("pytest-temp").get ()
            time.sleep (0.5)
        assert r.status_code == 404



